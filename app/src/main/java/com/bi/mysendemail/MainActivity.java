package com.bi.mysendemail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText et_selfName,et_selfEmail,et_selfPass,et_receiverName,et_receiverEmail,et_ccName,et_ccEmail,et_subject,
    et_firstContent,et_secondContent,et_thirdContent,et_firstProcess,et_secondProcess,et_thirdProcess;
    private Button btn_showPass,btn_preview,btn_sendNow,btn_sendTime;
    private TextView tv_sendResult,tv_sendPreview;
    private String selfEmail,selfName,selfPass;
    private String receiverName,receiverEmail;
    private String ccName,ccEmail;
    private String subject;
    private String firstContent,secondContent,thirdContent;
    private String firstProcess,secondProcess,thirdProcess;
    private boolean isShow=false;
    private SendEmail sendEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        sendEmail= new SendEmail();

    }

    private void initView() {
        tv_sendPreview= (TextView) findViewById(R.id.tv_preview);
        tv_sendResult= (TextView) findViewById(R.id.send_result);

        btn_showPass= (Button) findViewById(R.id.btn_showpass);
        btn_preview= (Button) findViewById(R.id.btn_preview);
        btn_sendNow= (Button) findViewById(R.id.btn_now_send);
        btn_sendTime= (Button) findViewById(R.id.btn_time_send);

        btn_showPass.setOnClickListener(this);
        btn_preview.setOnClickListener(this);
        btn_sendNow.setOnClickListener(this);
        btn_sendTime.setOnClickListener(this);
        //个人信息
        et_selfEmail= (EditText) findViewById(R.id.et_self_email);
        et_selfName=(EditText) findViewById(R.id.et_self_shortname);
        et_selfPass=(EditText) findViewById(R.id.et_self_emailpass);
        //接受者信息
        et_receiverEmail=(EditText) findViewById(R.id.et_receiver_email);
        et_receiverName=(EditText) findViewById(R.id.et_receiver_shortname);
        //抄送者的信息
        et_ccEmail=(EditText) findViewById(R.id.et_cc_email);
        et_ccName=(EditText) findViewById(R.id.et_cc_shortname);
        //邮件主题
        et_subject=(EditText) findViewById(R.id.et_email_subject);
        //工作计划
        et_firstContent=(EditText) findViewById(R.id.et_first_content);
        et_firstProcess=(EditText) findViewById(R.id.et_first_process);
        et_secondContent=(EditText) findViewById(R.id.et_second_content);
        et_secondProcess=(EditText) findViewById(R.id.et_second_process);
        et_thirdContent=(EditText) findViewById(R.id.et_third_content);
        et_thirdProcess=(EditText) findViewById(R.id.et_third_process);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_showpass:
                if(isShow){//隐藏密码
                    et_selfPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    isShow=false;
                }else{//显示密码
                    et_selfPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    isShow=true;
                }
                break;
            case R.id.btn_preview:
                getInfo();
                String preString=preView(true);
                tv_sendPreview.setText(Html.fromHtml(preString));
                break;

            case R.id.btn_now_send:
                getInfo();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //自定义邮件的主题
                        Date currentDate=new Date();
                        SimpleDateFormat format0 = new SimpleDateFormat("yyyy-MM-dd");
                        String time = format0.format(currentDate.getTime());//这个就是把时间戳经过处理得到期望格式的时间

                        String subject_temp= "【日报】 "+time+" "+selfName;
                        sendEmail.send(selfEmail,selfPass,selfName,receiverEmail,receiverName,ccEmail,ccName,subject_temp,preView(false));
                    }
                }).start();

                break;

            case R.id.btn_time_send:

                break;

        }

    }

    public void getInfo(){
        selfEmail=et_selfEmail.getText().toString();
        selfName=et_selfName.getText().toString();
        selfPass=et_selfPass.getText().toString();
        receiverName=et_receiverName.getText().toString();
        receiverEmail=et_receiverEmail.getText().toString();
        ccEmail=et_ccEmail.getText().toString();
        ccName=et_ccName.getText().toString();
        subject=et_subject.getText().toString();
        firstContent=et_firstContent.getText().toString();
        firstProcess=et_firstProcess.getText().toString();
        secondContent=et_secondContent.getText().toString();
        secondProcess=et_secondProcess.getText().toString();
        thirdContent=et_thirdContent.getText().toString();
        thirdProcess=et_thirdProcess.getText().toString();
    }

    private String preView(boolean isPreview) {
        String preString="";
        if(isPreview){
            Date currentDate_temp=new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String time_temp = format.format(currentDate_temp.getTime());//这个就是把时间戳经过处理得到期望格式的时间
            preString="发件人："+selfName +"<"+selfEmail+"><br><br>"+
                    "发送时间："+time_temp+"<br><br>"+
                    "收件人："+receiverName+"<"+receiverEmail+"><br><br>"+
                    "抄送："+ccName+"<"+ccEmail+"><br><br>";
        }

        Date currentDate=new Date();
        SimpleDateFormat format0 = new SimpleDateFormat("yyyy-MM-dd");
        String time = format0.format(currentDate.getTime());//这个就是把时间戳经过处理得到期望格式的时间

        preString+= "【日报】 "+time+" "+selfName +"<br><br>" +
                "■今日工作计划<br><br>";
        if(firstContent.length()!=0){
            preString+="    1、"+ firstContent+"<br><br>" ;
        }
        if(secondContent.length()!=0){
            preString+="    2、"+ secondContent+"<br><br>" ;
        }
        if(thirdContent.length()!=0){
            preString+="    3、"+ thirdContent+"<br><br>" ;
        }

        preString+= "■今日工作内容和实施状态<br><br>";
        if(firstProcess.length()!=0){
            preString+="    1、"+ firstContent+"-------------------------- "+firstProcess +"%<br><br>" ;
        }
        if(secondProcess.length()!=0){
            preString+="    2、"+ secondContent+"-------------------------- "+secondProcess +"%<br><br>" ;
        }
        if(thirdProcess.length()!=0){
            preString+="    3、"+ thirdContent+"-------------------------- "+thirdProcess +"%<br><br>" ;
        }
        preString+= "■明日工作计划<br><br>" +
                "■问题课题<br><br>" +
                "■问题原因及对策";

        return preString;
    }

}
